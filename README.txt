Author - Altaf Samnani

======================Technologies Used==========================
1) PHP
2) Mysql
3) Zend Framework 1.12.11
4) Bootstrap is used for responsive css
5) Checkout GIT URL
   https://samnani@bitbucket.org/samnani/sample.git

=======================Steps to execute==========================
1) Configure Apache

   Add entry to sites-available (Follow instructions in docs/SetupVHOST.txt)
   Enable site ex a2ensite sample
   Add entry to /etc/hosts

2) Import /data/db/locations.sql into Mysql Database

3) Set Database connections in /applications/configs/application.ini

   Example
   resources.db.params.host      = localhost
   resources.db.params.username  = root
   resources.db.params.password  = altaf12345
   resources.db.params.dbname    = sample

4) Assignment directory structure is stored in data/assignment/sample

5) Cache configuration is in application/Bootstrap.php

   I am using Zend_Cache_Backend_File and cache file is stored in /temp/

   There are various methods for caching, if you have memcache, apc server settings you can set it in application/Bootstrap.php

   1)Zend_Cache_Backend_File

   2)Zend_Cache_Backend_Memcached

   3)Zend_Cache_Backend_Apc etc

6) Make sure /temp directory is writable


================= Zend Framework Structure Explanation ==============
Current architecture is using ModelViewController framework.

1) Controllers
   Path - Application/controllers/IndexController.php
   Actions - indexAction   - This action reads given directory and index all the directory and files into database and displays it
           - displayAction - This action checks the cache data and if its not present then retireves it from the database
   Explanations
   I tried to keep controller as thin as possible.

2) Models/Mappers/DbTable
   models/LocationsMapper.php    - Variable manuplation is happening in /application/models/LocatioinsMapper.php
   models/Locations.php         - Standard Setter/Getter methods are in /application/models/Locations.php
   models/DbTable/Locations.php - Customized Query and Db Table Abstract

3) Views
   application/layouts/script/layout.phtml -  Its used for site wise html and styling
   application/views/index/index.phtml     -  Depends on the action wise html and styling
   application/views/helpers/Listing.php   -  Helper classes for rendering html

=================How did i configure Zend ?========================

1) Opend terminal & Create Zend Project
   zf create project sample

2) We want to ensure we have an XHTML DocType declaration for our application. To enable this, we have added DocType to view, it can be accessed through <?php echo $this->doctype() ?>
   Refer Bootstrap.php _initDoctype() for the same

3) Enable Layout
   zf enable layout

4) Create Db-Table
   zf create db-table Locations locations

5) Create Dbtable Mapper
   zf create model LocationsMapper

6) Edit the class LocationMapper and add the content according to docs

7) Create Model
   zf create model Locations

8) Create controller/views
   zf create controller



