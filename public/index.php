<?php
/**
* Index.php Control starts from here
*
* @access     public
* @copyright  Copyright (c) 2005-2015 AltafSampleCode
* @author     Altaf S
* @license    http://in.linkedin.com/in/altafsamnani   AltafSampleCode
* @version    Release: 1
* @link       http://localhost/inddex/
* @since      Class available since Release 1
*
*  Change History   :
*  Mod#            Date            Who             Description
*  -----------     -----------     -----------     -----------
*  01              24-Mar-2015     Altaf S         Created Bootstrap File
*/

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);
$application->bootstrap()
            ->run();