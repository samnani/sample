<?php

/**
* IndexController
*
* @access     public
* @copyright  Copyright (c) 2005-2015 AltafSampleCode
* @author     Altaf S
* @license    http://in.linkedin.com/in/altafsamnani   AltafSampleCode
* @version    Release: 1
* @link       http://localhost/inddex/
* @since      Class available since Release 1
*
* Mod#            Date            Who             Description
* -----------     -----------     -----------     -----------
* 01				24-Mar-2015		Altaf S		    Created Display Directory Structure
*/

class IndexController extends Zend_Controller_Action
{
	/**
     * Path of the directory
     *
     * @var string
     */
	private $path 	= 	'../data/assignment/'	;

	/**
     * Path of the directory
     *
     * @var string
     */
    public 	function init( )
    {
        /* Initialize action controller here */
    }

    /**
     * indexAction - This action reads given directory and index all the directory and
     * files into database and displays it
     *
     *
     * @return void
     */
    public function indexAction( )
    {

    	$locationsMapper 		=	 new Application_Model_LocationsMapper( );

    	$this->view->locations 	= $locationsMapper->indexDirectoryTree($this->path);

    }

    /**
     * displayAction - This action checks the cache data and
     * if its not present then retireves it from the database
     *
     * @return void
     */
    public function displayAction( )
    {

        $locationsMapper 		= new Application_Model_LocationsMapper( );

        $this->view->locations 	= $locationsMapper->fetchDirectoryTree( );
    }
}
