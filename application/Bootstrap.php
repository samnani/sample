<?php
/**
 * Bootstrap Functionality
 *
 * @access     public
 * @copyright  Copyright (c) 2005-2015 AltafSampleCode
 * @author     Altaf S
 * @license    http://in.linkedin.com/in/altafsamnani   AltafSampleCode
 * @version    Release: 1
 * @link       http://localhost/inddex/
 * @since      Class available since Release 1
 *
 *  Change History   :
 *  Mod#            Date            Who             Description
 *  -----------     -----------     -----------     -----------
 *  01              24-Mar-2015     Altaf S         Created Bootstrap File
 */

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{


  	protected function _initDoctype( )
    {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->doctype('XHTML1_STRICT');
    }

    protected function _initCache( )
    {

	    $frontend= array('lifetime' => 7200,
	        			 'automatic_serialization' => true
	    				);

	    $backend= array(
	        'cache_dir' => '../temp/'
	    );

	    $cache = Zend_Cache::factory('core',
						             'File',
						             $frontend,
						             $backend
						    );

	    Zend_Registry::set('cache', $cache);

	}

}

