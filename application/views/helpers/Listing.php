<?php

/**
 * Listing View Helper
 *
 * @access     public
 * @copyright  Copyright (c) 2005-2015 IENS
 * @author     Altaf S
 * @license    http://www.iens.nl   IENS
 * @version    Release: 1
 * @link       http://www.iens.nl/index/index
 * @since      Class available since Release 1
 *
 *  Change History   :
 *  Mod#            Date            Who             Description
 *  -----------     -----------     -----------     -----------
 *  01              24-Mar-2015     Altaf S         Created LocationMapper
 */

class Application_Views_Helpers_Listing extends Zend_View_Helper_Abstract
{
	public function listing( $locations = '../iens/' , $startingLevel = 0 )
	{
		echo "<ul>\n";

		foreach ($locations as $key => $location)
        {
            if (count($location['children']) > 0)
            {
                echo "<li>{$location['location_name']}\n";

                $this->listing( $location['children'], $startingLevel + 1 );

                echo "</li>\n";
            }
            else
            {
                echo "<li>{$location['location_name']}</li>\n";
            }
        }

        echo "</ul>";

        return true;
	}
}