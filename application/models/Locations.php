<?php
/**
* Location Model
*
* @access     public
* @copyright  Copyright (c) 2005-2015 AltafSampleCode
* @author     Altaf S
* @license    http://in.linkedin.com/in/altafsamnani   AltafSampleCode
* @version    Release: 1
* @link       http://localhost/inddex/
* @since      Class available since Release 1
*
*  Change History   :
*  Mod#            Date            Who             Description
*  -----------     -----------     -----------     -----------
*  01              24-Mar-2015     Altaf S         Created Location Model
*/
class Application_Model_Locations
{
    /**
     * Name of the directory/file
     *
     * @var string
     */
    protected $_location_name;

    /**
     * Created date of the record
     *
     * @var int
     */
    protected $_created;

    /**
     * Parent id of the directory / file
     *
     * @var int
     */
    protected $_parent_id;

    /**
     * Id of the record
     *
     * @var int
     */
    protected $_id;

    /**
     * Constructor.
     *
     * @param  array $options Options used in setting options
     * @return void
     */
    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    /**
     * Set method of the table locations
     *
     * @return function
     */
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Sample Location property');
        }
        $this->$method($value);
    }

    /**
     * Get method of the table locations
     *
     * @return function
     */
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Sample Location property');
        }
        return $this->$method( );
    }

    /**
     * Set options for the table locations
     * @param array $options options of array
     * @return Application_Model_Locations
     */
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    /**
     * Set options for the table locations
     * @param string $text location name
     * @return Application_Model_Locations
     */
    public function setLocationName($text)
    {
        $this->_location_name = (string) $text;
        return $this;
    }

    /**
     * Get location name
     * @return Application_Model_Locations
     */
    public function getLocationName( )
    {
        return $this->_location_name;
    }

    /**
     * Set options for the table locations
     * @param int $parent_id get parent id
     * @return Application_Model_Locations
     */
    public function setParentId($parent_id)
    {
        $this->_parent_id = (string) $parent_id;
        return $this;
    }

    /**
     * Set options for the table locations
     * @param array $options options of array
     * @return int $this->_parent_id gives back the parent id
     */
    public function getParentId( )
    {
        return $this->_parent_id;
    }

    /**
     * Set created date
     * @param timestamp $ts timestamp
     * @return Application_Model_Locations
     */
    public function setCreated($ts)
    {
        $this->_created = $ts;
        return $this;
    }

    /**
     * Get created date for record
     * @return timestamp $this->_created gives back created date
     */
    public function getCreated( )
    {
        return $this->_created;
    }

    /**
     * Set id for record
     * @param int $id sets id of record
     * @return Application_Model_Locations
     */
    public function setId($id)
    {
        $this->_id = (int) $id;
        return $this;
    }

    /**
     * Get id of the record
     * @return int gets the id of the record
     */
    public function getId( )
    {
        return $this->_id;
    }
}

