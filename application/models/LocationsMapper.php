<?php
/**
* Location Mapper
*
* @access     public
* @copyright  Copyright (c) 2005-2015 AltafSampleCode
* @author     Altaf S
* @license    http://in.linkedin.com/in/altafsamnani   AltafSampleCode
* @version    Release: 1
* @link       http://localhost/inddex/
* @since      Class available since Release 1
*
*  Change History   :
*  Mod#            Date            Who             Description
*  -----------     -----------     -----------     -----------
*  01              24-Mar-2015     Altaf S         Created LocationMapper
*/

class Application_Model_LocationsMapper
{

    /**
    * Ignore File Extensions
    *
    * @var array
    */
    private    	$ignoreFileExtensions = 	array('cgi-bin', '.', '..');

    /**
	* set of dates company remains closed
	*
	* @var Zend_Db_Table_Abstract
	*/
    protected 	$_dbTable;


    /**
    * Data Array
    *
    * @var array
    */
    protected   $treeData;

    /**
     * Sets dbTable Instance
     *
     *
     * @param obj $this
     *
     * @return $this
     * @access public
     *
     */
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable))
        {
            $dbTable = new $dbTable( );
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract)
        {
            throw new Exception('Invalid table data gateway provided');
        }

        $this->_dbTable = $dbTable;

        return $this;
    }

    /**
     * Get dbTable Instance
     *
     *
     * @param None
     *
     * @return obj $this->_dbTable db table instance
     * @access public
     *
     */
    public function getDbTable( )
    {
        if (null === $this->_dbTable)
        {
            $this->setDbTable('Application_Model_DbTable_Locations');
        }

        return $this->_dbTable;
    }

    /**
     * Insert or Udpate the row
     *
     *
     * @param obj  Application_Model_Locations
     *
     * @return int Returns integer value
     * @access public
     *
     */
    public function save(Application_Model_Locations $locations)
    {
        $data = array(
            'parent_id'   => $locations->getParentId( ),
            'location_name' => $locations->getLocationName( ),
            'created' => date('Y-m-d H:i:s'),
       );

        if (null === ($id = $locations->getId( )))
        {
            unset($data['id']);
            return $this->getDbTable( )->insert($data);
        }
        else
        {
            return $this->getDbTable( )->update($data, array('id = ?' => $id));
        }
    }

    /**
     * Find the row
     *
     * @param int  $id Id of the row
     * @param obj  Application_Model_Locations
     *
     * @return array $entries returns matched  rows
     * @access public
     *
     */
    public function find($id, Application_Model_Locations $locations)
    {
        $result = $this->getDbTable( )->find($id);
        if (0 == count($result))
        {
            return;
        }
        $row = $result->current( );
        $locations->setId($row->id)
                  ->setParentId($row->parent_id)
                  ->setLocationName($row->location_name)
                  ->setCreated($row->created);
    }

    /**
     * Fetch all rows
     *
     *
     * @return array $entries returns total rows
     * @access public
     *
     */
    public function fetchAll( )
    {
        $resultSet = $this->getDbTable( )->fetchAll( );

        $entries   = array( );

        foreach ($resultSet as $row)
        {
            $entry = new Application_Model_Locations( );
            $entry->setId($row->id)
                  ->setParentId($row->parent_id)
                  ->setLocationName($row->location_name)
                  ->setCreated($row->created);
            $entries[] = $entry;
        }
        return $entries;
    }

    /**
     * Truncate table
     *
     *
     * @return None
     * @access public
     *
     */
	public function truncateTable( )
	{
	    $this->getDbTable( )->delete("1");
	}

    /**
     * Index Directories/Files and Insert Values
     *
     * @param string  $categories directory path to be scanned
     *
     * @return array  Record set containt hierarchy data
     * @access public
     *
     */
    public function indexDirectoryTree ($path)
    {
        if(is_dir("$path"))
        {
            $this->truncateTable( );

            $this->scanAndInsertDirectoryTree( $path );

            return $this->getTreeData( );
        }

        return false;
    }

	/**
     * Scan Directories/Files and Insert Values
     *
     * @param string  $categories directory path to be scanned
     * @param int   $startingLevel depth id
     *
     * @return None
     * @access public
     *
     */
    public function scanAndInsertDirectoryTree($path, $startingLevel = 0)
    {
        // Open the directory to the handle $dh
	    $dh = @opendir($path);

        // Loop through the directory
	    while(false !== ($file = readdir($dh)))
	    {

	        if(!in_array($file, $this->ignoreFileExtensions))
	        {

	            $spaces = 	str_repeat('&nbsp;', ($startingLevel * 4));

	            // Just to add spacing to the list, to better
	            // show the directory tree.
	            $entry 	= 	new Application_Model_Locations( );

            	$entry->setParentId($startingLevel)
                      ->setLocationName($file);

                $dbId 	=	$this->save($entry);

               	$treeStructure[ $dbId ] 	= 	array (	'id' 			=> $dbId,
            											'parent_id' 	=> $startingLevel,
            											'location_name' => $file,
            											'children'      => array( )
                							   			 );

               	$this->treeData [ ]         =   $treeStructure[ $dbId ];

	            if(is_dir("$path/$file"))
	            {

	            	// Re-call this same function but on a new directory.
	                $this->scanAndInsertDirectoryTree("$path/$file", ($dbId));

	            }
	        }

		}

    	closedir($dh);
    }

    /**
     * Scan Option - Gets the tree data in multidimensional Array
     *
     *
     * @return array $organizedTree Multidimensional set for directory structure
     * @access public
     *
     */
    public function getTreeData( )
    {
        $organizedTree =   NULL;

    	$cache         = Zend_Registry::get('cache');

    	$cache->remove('listing');

        if( count($this->treeData))
        {
    	   foreach ($this->treeData as $row)
            {
	    	  if($row['parent_id'])
		 	    {
		  		  $this->addToTree($organizedTree, $row['parent_id'], $row);
		 	    }
		 	    else
		 	    {
		  		  $organizedTree[$row['id']] = $row;
		 	    }
		    }
        }

		return $organizedTree;
    }

    /**
     * Render Tree from Db Data
     * Retrieves data from DB and and returns multidimensional array
     *
     *
     * @return array $organizedTree Multidimensional set for directory structure
     * @access public
     *
     */
    public function fetchDirectoryTree( )
    {
        $organizedTree  = NULL;

        $cache          = Zend_Registry::get('cache');

        //From Non Cache/ Fetch Db Record
    	if( ($result = $cache->load('listing')) === false )
    	{
            echo '<p class="bg-primary">From Non Cache</p>';
    		$resultSet = $this->getDbTable( )->fetchAll( );

    		$cache->save($resultSet, 'listing');
    	}
    	else
    	{
            //From Cache
            echo '<p class="bg-primary">From Cache</p>';
    		$resultSet = $result;
    	}

        foreach ($resultSet as $origRow)
        {
        	$row =	array(	'id' 			=> $origRow->id
        				 ,  'parent_id' 	=> $origRow->parent_id
        				 ,	'location_name' => $origRow->location_name
        				 ,  'children'      => array( )
        				) ;

		 	if($row['parent_id'])
		 	{
		  		$this->addToTree($organizedTree, $row['parent_id'], $row);
		 	}
		 	else
		 	{
		  		$organizedTree[$row['id']] = $row;
		 	}

		}

		return $organizedTree;
    }

    /**
     * Recursive function to store directory hierarchy
     *
     * @param obj  $parentTree Tree object reference
     * @param int  $pid row parent id
     * @param
     *
     * @return array $parentTree one set of directory structure
     * @access public
     *
     */

    public function addToTree(&$parentTree, $pid, $row)
    {
 		foreach($parentTree as $parentID => $parentCategory)
 		{
  			if($parentID == $pid)
  			{
   				return $parentTree[$parentID]['children'][$row['id']] = $row;
  			}
  			else
  			{
   				$this->addToTree ($parentTree[$parentID]['children'], $pid, $row);
  			}
 		}
	}
}

