<?php
/**
* Location DbTable Model
*
* @access     public
* @copyright  Copyright (c) 2005-2015 AltafSampleCode
* @author     Altaf S
* @license    http://in.linkedin.com/in/altafsamnani   AltafSampleCode
* @version    Release: 1
* @link       http://localhost/inddex/
* @since      Class available since Release 1
*
*  Change History   :
*  Mod#            Date            Who             Description
*  -----------     -----------     -----------     -----------
*  01              24-Mar-2015     Altaf S         Created DbTable Location
*/
class Application_Model_DbTable_Locations extends Zend_Db_Table_Abstract
{

    protected $_name = 'locations';


}

